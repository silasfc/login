package br.mp.mpto.loginexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class RecycleViewList extends AppCompatActivity {

    ArrayList<Food> dataSet = null;
    RecyclerView recyclerView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_recycle_view_list);

        // 1 - Cria e preenche o DataSet
        this.createDataSet();

        // 2 - Busca a referencia para o RecyclerView e ajusta alguns parametros
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        // 3 - Instancia um objeto adapter que recebe o dataset
        ListAdapter listAdapter = new ListAdapter(this, dataSet);

        // 4 - Vincula o adapter com a RecyclerView
        recyclerView.setAdapter(listAdapter);
    }

    public void createDataSet() {
        dataSet = new ArrayList<Food>();
        dataSet.add(new Food(2, "California", R.drawable.california));
        dataSet.add(new Food(3, "Mussarela", R.drawable.mussarela));
        dataSet.add(new Food(4, "Portuguesa", R.drawable.portuguesa));
        dataSet.add(new Food(5, "Quatro Queijos", R.drawable.quatro_queijos));
        dataSet.add(new Food(6, "Salada de Frutas", R.drawable.salada_de_frutas));
        dataSet.add(new Food(7, "Sorvete", R.drawable.sorvete));
        dataSet.add(new Food(8, "Sucos", R.drawable.sucos));
    }
}
