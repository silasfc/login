package br.mp.mpto.loginexample;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_login);
    }

    @Override
    public void onActivityResult(int cod, int res, Intent intent) {
        if (res == Activity.RESULT_OK) {
            if (cod == 1020) {
                boolean userChoice = intent.getExtras().getBoolean("notifications");
                PersistenceManager.getPersistenceManager().save(this, userChoice);
            }
        }
    }

    // Metodo utilizado para a captura de eventos
    public void handleButton(View button) {
        EditText login = findViewById(R.id.field_login);
        EditText password = findViewById(R.id.field_password);


        if (password.getText().toString().equals("tce")) {
            // Cria uma intencao para troca de tela
            Intent intent = new Intent(this, Main.class);

            // Cria um dicionario e seta um dado
            Bundle bundle = new Bundle();
            bundle.putString("user", login.getText().toString());

            // Embute o dicionario na nova Intent
            intent.putExtras(bundle);

            // Empilha a nova tela
            startActivityForResult(intent, 1020);
        }
        else {
            Toast.makeText(this, "Login inválido", Toast.LENGTH_SHORT).show();
        }
    }
}
