package br.mp.mpto.loginexample;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

public class Profile extends Fragment implements DialogInterface.OnClickListener {

    int[] images = {
            R.drawable.california,
            R.drawable.mussarela,
            R.drawable.portuguesa,
            R.drawable.quatro_queijos,
            R.drawable.sucos,
            R.drawable.sorvete,
            R.drawable.salada_de_frutas
    };

    TextView userName;

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_profile);

        Switch aSwitch = findViewById(R.id.profile_switch_notifications);

        userName = findViewById(R.id.profile_username);
        boolean userChoice = PersistenceManager.getPersistenceManager().getStored(this);
        Log.i("INFO", Boolean.toString(userChoice));
        aSwitch.setChecked(userChoice);

        Bundle bundle = getIntent().getExtras();
        String user = bundle.getString("user");

        userName.setText(getResources().getText(R.string.welcome) + ", " + user + "!");

        LinearLayout linearLayout = findViewById(R.id.profile_linearLayout);

        for (int i=0; i < images.length; i++) {
            View cell = LayoutInflater.from(this).inflate(R.layout.layout_slider, linearLayout, false);
            ImageView imageView = cell.findViewById(R.id.slider_imageView);
            imageView.setImageResource(images[i]);
            linearLayout.addView(cell);
        }
    }*/

    @Override
    public void onClick(DialogInterface dialogInterface, int codButton) {
        if (codButton == DialogInterface.BUTTON_POSITIVE) {
            Switch choice = findViewById(R.id.profile_switch_notifications);

            Bundle bundle = new Bundle();
            bundle.putBoolean("notifications", choice.isChecked());

            Intent intent = new Intent();
            intent.putExtras(bundle);

            // this.setResult(Activity.RESULT_OK, intent);

            // Finalizar esta Activity
            // finish();
        }
    }

    /*public void handleLogoff(View button) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("AVISO!");
        alert.setIcon(R.mipmap.ic_launcher_round);
        alert.setMessage(getResources().getText(R.string.would_you_really_like_logoff) + "?");
        alert.setPositiveButton("Sim", this);
        alert.setNegativeButton("Não", this);
        alert.show();
    }*/

    /*@Override
    public void onBackPressed() {
        handleLogoff(findViewById(R.id.profile_button_logoff));
    }*/
}
