package br.mp.mpto.loginexample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<FoodHolder> {

    private Context context;
    private ArrayList<Food> foodList;

    public ListAdapter(Context context, ArrayList<Food> foodList) {
        this.context = context;
        this.foodList = foodList;
    }

    // Quantas celulas preciso criar?
    @Override
    public int getItemCount() {
        return foodList.size();
    }

    // Metodo que cria N celulas
    @Override
    public FoodHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cell = LayoutInflater.from(context).inflate(R.layout.layout_cell_food, parent, false);

        FoodHolder foodHolder = new FoodHolder(cell);

        return foodHolder;
    }

    // Metodo que define com que info a celula sera preenchida
    @Override
    public void onBindViewHolder(FoodHolder holder, int position) {
        // Busca o objeto (Modelo) no ArrayList na posicao
        Food food = foodList.get(position);

        // Preenche a celula com os dados do Modelo
        holder.getImage().setImageResource(food.getImage());
        holder.getName().setText(food.getName());
        holder.getRating().setRating(food.getRating());
    }
}
