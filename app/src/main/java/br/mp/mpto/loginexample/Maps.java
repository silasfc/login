package br.mp.mpto.loginexample;

import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class Maps extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng tceTO = new LatLng(-10.177710, -48.332699);
        mMap.addMarker(new MarkerOptions().position(tceTO).title("TCE-TO"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(tceTO));
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setOnMarkerClickListener(this);

        CameraUpdate zoom = CameraUpdateFactory.newLatLngZoom(tceTO, 15);
        mMap.moveCamera(zoom);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Geocoder geocoder = new Geocoder(this);
        try {
            List<Address> results = geocoder.getFromLocationName("MPE-TO", 1);

            double la = results.get(0).getLatitude();
            double lo = results.get(0).getLongitude();

            Log.i("INFO", "MPE-TO is in: " + la + " " + lo);
        }
        catch (Exception e) {
            e.printStackTrace();
            Log.i("INFO", "MPE-TO not found");
        }

        Log.i("INFO", "Marker clicked");
        return true;
    }
}
