package br.mp.mpto.loginexample;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class PersistenceManager {
    private static PersistenceManager manager = null;
    private PersistenceManager() { }

    public static PersistenceManager getPersistenceManager() {
        if (manager == null) {
            manager = new PersistenceManager();
        }

        return manager;
    }

    public boolean getStored(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);

        if (preferences.contains("notifications")) {
            return preferences.getBoolean("notifications", false);
        }

        return false;
    }

    public void save(Context context, boolean b) {
        SharedPreferences preferences = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        Editor editor = preferences.edit();
        editor.putBoolean("notifications", b);
        editor.commit();
    }
}
