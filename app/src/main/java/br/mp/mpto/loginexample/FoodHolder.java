package br.mp.mpto.loginexample;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class FoodHolder extends RecyclerView.ViewHolder {
    private ImageView image;
    private TextView name;
    private RatingBar rating;

    public FoodHolder(View cell) {
        super(cell);
        image = cell.findViewById(R.id.recyclerView_image);
        name = cell.findViewById(R.id.recyclerView_description);
        rating = cell.findViewById(R.id.recyclerView_rating);
    }

    public ImageView getImage() {
        return image;
    }

    public TextView getName() {
        return name;
    }

    public RatingBar getRating() {
        return rating;
    }
}
