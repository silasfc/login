package br.mp.mpto.loginexample;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class IntentTests extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_intent_tests);
    }

    public void handleCall(View button) {
        Uri uri = Uri.parse("tel:32333455");
        Intent intent = new Intent(Intent.ACTION_CALL, uri);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    public void handleGoWeb(View button) {
        Uri uri = Uri.parse("http://google.com");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public void handleGoMap(View button) {
        String rota = "http://maps.google.com/maps?saddr=-10.184519,-48.335203&daddr=-10.177710,-48.332699";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(rota));
        startActivity(intent);
    }
}
