package br.mp.mpto.loginexample;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

public class Main extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private FragmentManager manager = null;
    private FragmentTransaction transaction = null;
    private ProfileFragment profileFragment = null;
    private RecycleViewList menu = null;
    private Maps about = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);

        BottomNavigationView navigationView = findViewById(R.id.main_navigationView);
        navigationView.setOnNavigationItemSelectedListener(this);
        manager = getSupportFragmentManager();
    }

    public void showProfile() {
        profileFragment = new ProfileFragment();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.main_frameLayout, profileFragment);
        transaction.commit();
    }

    public void showMenu() {
        /*menu = new RecycleViewList();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.main_frameLayout, menu);
        transaction.commit();*/
    }

    public void showAbout() {
        /*about = new Maps();
        transaction = manager.beginTransaction();
        transaction.replace(R.id.main_frameLayout, about);
        transaction.commit();*/
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profile:
                Log.i("INFO", "Profile");
                this.showProfile();
                break;
            case R.id.menu:
                Log.i("INFO", "Menu");
                this.showMenu();
                break;
            case R.id.about:
                Log.i("INFO", "About");
                this.showAbout();
                break;
            default:
                Log.i("INFO", "Default");
                this.showProfile();
        }

        return true;
    }
}
